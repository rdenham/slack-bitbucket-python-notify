#coding:utf-8
import os
import hashlib
import logging

import flask
import slackweb
import os

## get our environment things:
slackWebHookUrl = os.environ.get('SLACK_WEBHOOKURL')
slackChannel = os.environ.get("SLACK_CHANNEL")
if not slackChannel.startswith('#'):
    slackChannel = "#%s" % slackChannel

slack = slackweb.Slack(url=slackWebHookUrl)

app = flask.Flask(__name__)

# Logging configuration
stderr_log_handler = logging.StreamHandler()
app.logger.addHandler(stderr_log_handler)
app.logger.setLevel(logging.DEBUG)

def notify_slack(payload):
    """
    Notify my slack channel in a particular way
    see https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html
    """
    ## designed for issues for the moment
    attachments = {"fallback": "Some message from my repo",
                   "pretext": "Issue from repo",
                   "color": "#36a64f",
                   "author_name": payload['actor']['username'],
                   "title": "Issue #%d: %s" % (payload['issue']['id'],
                                              payload['issue']['title']),
                   "title_link": payload['issue']['links']['html']['href']}
    # there are three types of issue changes, create, update, comment
    # If it has a changes key its an update
    # if it has comment but not changes its a comment
    # it it doesn't have a comment its a create
    if "comment" not in payload:
        attachments['text'] = "An issue was created: %s" % payload['issue']['content']['raw']
    elif "comment" in payload and "changes" in payload:
        attachments['text'] = "issue updated: %s" % payload['comment']['content']['raw']
    else:
        attachments['text'] = "new comment: %s" % payload['comment']['content']['raw']
    slack.notify(attachments=[attachments], channel=slackChannel)
   

@app.route("/", methods=("GET",))
def webhook_get_handler():
    return flask.Response(status=200)


@app.route("/", methods=("POST",))
def webhook_post_handler():
    payload = flask.request.json
    print(payload.keys())
    if 'issue' in payload:
        # we only do issues for now.
        print(payload['issue'])
        notify_slack(payload) 
    app.logger.info("Received Notification") 

    # Here, you should do whatever you feel like doing with the payload

    return flask.Response(status=202)


@app.before_request
def log_request():
    app.logger.debug("Received request: %s %s", flask.request.method, flask.request.url)


@app.before_request
def validate_json_payload():
    if flask.request.method == "GET":
        return
    if flask.request.json is None:
        return flask.Response(status=400)


if __name__ == '__main__':
    app.run(debug=True)
